"""Events API"""
import logging
from fastapi import FastAPI

logging.basicConfig(level=logging.INFO)
api = FastAPI(docs_url="/")
LOGGER = logging.getLogger(__name__)

@api.get("/events/telegram")
async def handle_televram_event():
    LOGGER.info("telegram message received")
