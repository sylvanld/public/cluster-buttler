FROM python:3.8-alpine

WORKDIR /app

COPY requirements/prod.txt /tmp/requirements
RUN pip install --no-cache-dir -r /tmp/requirements

COPY buttler/ /app/buttler
COPY setup.py /app/setup.py

EXPOSE 80
CMD ["uvicorn", "buttler.events.api:api", "--host=0.0.0.0", "--port=80"]
